# coding=utf-8

"""
Example of combining use of servo and gripper classes for a gripper which can spin its fingers.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import traceback

from henschel_servo_control import EtherAddress
from henschel_servo_control.henschel_servo import HenschelServo
from henschel_servo_control.gripper import Gripper


# The motors are assumed to have been set up on the 192.168.1.0/24 network. Add
# an addres on that network with the root or sudo command, assuming
# the name of the network interface is "eth0":

# # ip addr add 192.168.1.1/24 dev eth0

# Port seems to be fixed to 1000, even though the web interface
# says it has been changed.
addr_spin = EtherAddress('192.168.1.102', 1000)
addr_grip = EtherAddress('192.168.1.101', 1000)


try:
    spin = HenschelServo(addr_spin)
    spin.start()
except Exception:
    print('Could not start spinner servo: ')
    traceback.print_exc()

try:
    grip = Gripper(addr_grip)
    grip.start()
except Exception:
    print('Could not start gripper servo: ')
    traceback.print_exc()
