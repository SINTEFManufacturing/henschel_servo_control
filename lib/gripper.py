# coding=utf-8

"""
Convenience class where a Henschel servo is used for controlling the action of a griper.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import dataclasses

import numpy as np

from . import EtherAddress
from .henschel_servo import HenschelServo


@dataclasses.dataclass
class Gripper(HenschelServo):
    addr: EtherAddress
    travel_torque: float = 0.025
    clamp_torque: float = 0.1
    release_torque: float = 0.5

    def __post_init__(self):
        HenschelServo.__init__(self, self.addr)
        self._last_close_pos = None

    def __hash__(self):
        return id(self)

    def close(self, speed=10, torque=None):
        if torque is None:
            torque = self.travel_torque
        self.seek_contact(vel=speed, torque=torque, ret_dist=2*np.pi)
        self._last_close_pos = self.pos

    def open(self, speed=10, torque=None):
        if torque is None:
            torque = self.travel_torque
        self.seek_contact(vel=-speed, torque=torque, ret_dist=2*np.pi)

    def clamp(self, torque=None):
        if torque is None:
            torque = self.clamp_torque
        self.close()
        self.seek_contact(vel=2, torque=torque)

    def release(self, torque=None):
        if torque is None:
            torque = self.release_torque
        if self._last_close_pos is not None:
            self.set_cmd_pos(self._last_close_pos, speed=5, torque=torque)
        else:
            p_start = self.pos
            torque = self.min_torque
            while p_start - self.pos < 1:
                print(self.pos, torque)
                torque += 0.01
                self.set_cmd_vel(-1.0, torque)
                for i in range(10):
                    self.wait_for_ticket()
            self.vel = 0.0
