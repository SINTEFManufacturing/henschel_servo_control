# coding=utf-8

"""Interface to the Henschel servo controller over Ethernet.

Make sure to configure the motor to emit "BinaryTicket" telegrams,
which contains more information than the default "HDriveTicket". This
can be set in the web interface to the motor controller at the
following location: Motor Settings -> Communication -> Host
Communication.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import socket
import time
import threading
import dataclasses

import numpy as np

from . import (EtherAddress,
               BinaryTicket, BinaryTicketShort, HDriveTicket,
               ControlMode
               )


class HenschelServo(threading.Thread):

    def __init__(
            self,
            addr: EtherAddress,
            tcp: bool = True,  # It seems that only TCP-connections work!
            min_torque: float = 0.05,  # [Nm] Minimum torque for sustained travel
            max_torque: float = 1.0,  # [Nm] Maximum torque for motor
            default_torque: float = None,  # [Nm] Default travel torque
            default_speed: float = 1.0,  # [rad/s] Default position commands speed
            pos_tol: float = 0.01,  # [rad] Tolerance for obtaining a position
    ):
        threading.Thread.__init__(self)
        self.addr = addr
        self.tcp = tcp
        self.min_torque = min_torque
        self.max_torque = max_torque
        self.default_torque = default_torque
        self.default_speed = default_speed
        self.pos_tol = pos_tol

        self.daemon = True
        self._connect()
        self._ticket_lock = threading.Lock()
        self._ticket_event = threading.Condition()
        self.__stop = False
        self._p_high = None
        self._p_low = None
        self._stroke = 45.0
        # Ensure a reasonable default torque for travel
        if self.default_torque is None:
            self.default_torque = 0.5 * (self.min_torque + self.max_torque)
        self._speed = 0.0

    def _connect(self):
        if self.tcp:
            self._sock = socket.socket(socket.AF_INET,
                                       socket.SOCK_STREAM)
            self._sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
            self._sock.connect(self.addr)
        else:
            self._sock = socket.socket(socket.AF_INET,
                                       socket.SOCK_DGRAM)
            self._sock.bind(('0.0.0.0', self.addr[1]))

    def _send(self, cmd):
        self.wait_for_ticket()
        if self.tcp:
            self._sock.send(bytes(cmd, 'utf8'))
        else:
            self._sock.sendto(bytes(cmd, 'utf8'), self._recv_addr)

    # NOT WORKING CORRECTLY
    # def reset(self):
    #     """Reset and reconnect to servo."""
    #     self._send(f'<system mode="{SysFunc.restart_motor}">')
    #     time.sleep(1)
    #     self._connect()

    def wait_for_ticket(self):
        with self._ticket_event:
            self._ticket_event.wait()

    def set_cmd_vel(self,
                    vel: float,
                    torque: float = None,  # Nm
                    timeout: float = None):
        self._cmd_vel = vel
        if torque is None:
            torque = self.default_torque
        cmd = (f'<control pos="0" ' +
               f'speed="{np.round(60 * vel / (2 * np.pi)):.0f}" ' +
               f'torque="{np.abs(np.round(1000 * torque)):.0f}" ' +
               f'mode="{ControlMode.vel}" acc="10000" decc="10000"/>'
               )
        # print(cmd)
        self._send(cmd)

    def get_cmd_vel(self):
        # Alternatively self._ticket.cmd_vel
        return self._cmd_vel

    def get_act_vel(self):
        return self._ticket.act_vel

    vel = property(get_act_vel, set_cmd_vel)

    def get_act_pos(self):
        return self._ticket.act_pos

    def set_cmd_pos(self,
                    pos: float,
                    speed: float = None,
                    torque: float = None,
                    relative: bool = False):
        self._cmd_pos = pos
        if relative:
            self._cmd_pos += self.pos
        if speed is not None:
            self._speed = speed
        else:
            self._speed = self.default_speed
        if torque is None:
            torque = self.default_torque
        cmd = (
            f'<control pos="{np.round(10 * np.rad2deg(pos)):.0f}" ' +
            f'speed="{np.round(60 * self._speed / (2 * np.pi)):.0f}" ' +
            f'torque="{np.abs(np.round(1000 * torque)):.0f}" ' +
            f'mode="{ControlMode.pos}" acc="10000" decc="10000"/>'
        )
        self._send(cmd)

    pos = property(get_act_pos, set_cmd_pos)

    def stop(self):
        self._sock.send(bytes(f'<control pos="0" mode="{ControlMode.stop}"/>',
                              'utf8'))
        self.__stop = True

    def get_ticket(self, wait=True):
        with self._ticket_event:
            self._ticket_event.wait()
        with self._ticket_lock:
            return dataclasses.replace(self._ticket)

    ticket = property(get_ticket)

    def seek_contact(self,
                     vel: float,
                     torque: float = None,
                     ret_dist: float = 0.0,
                     ret_spd: float = None):
        if ret_spd is None:
            ret_spd = np.abs(vel)
        if torque is None:
            torque = self.min_torque
        flt_vel = vel
        self.set_cmd_vel(vel, torque)
        # Allow for start of motion
        for i in range(5):
            self.wait_for_ticket()
        while True:
            self.wait_for_ticket()
            # cmd_vel = self._ticket.cmd_vel
            act_vel = self._ticket.act_vel
            flt_vel = 0.8 * flt_vel + 0.2 * act_vel
            vel_dev_rel = np.abs((flt_vel - vel) / vel)
            # print(cmd_vel, act_vel, flt_vel, seek_vel, vel_dev_rel)
            if vel_dev_rel > 0.9:
                break
        self.vel = 0.0
        print('Contact found')
        # Return 'ret_dist' distance to escape stiction
        if ret_dist > 0.0:
            print('Reversing')
            self.set_cmd_pos(self.pos - np.sign(vel) * ret_dist,
                             speed=ret_spd, torque=2*torque)
            self.wait_for_ticket()
            while (np.abs(self._ticket.act_pos - self._ticket.cmd_pos)
                   > self.pos_tol):
                time.sleep(0.1)
        print('Done')

    # def seek_high_mec_stop(self, seek_speed=5):
    #     # Find contact in positive direction and return 1 radian.
    #     self.seek_contact(vel=np.abs(seek_speed), ret_dist=1.0)
    #     self._p_high = self.pos
    #     if self._p_low is not None:
    #         self._stroke = self._p_high - self._p_low

    # def seek_low_mec_stop(self, seek_speed=5, zero=True):
    #     # Find contact in negative direction and return 1 radian.
    #     self.seek_contact(vel=-np.abs(seek_speed), ret_dist=1.0)
    #     self._p_low = self.pos
    #     if zero:
    #         p_enc = self._p_low
    #         self._p_low = 0.0
    #         self.wait_for_ticket()
    #         self._send(f'<system mode="{SysFunc.position_reset}">')
    #         if self._p_high is not None:
    #             self._p_high -= p_enc
    #     if self._p_high is not None:
    #         self._stroke = self._p_high - self._p_low

    # def seek_stroke_mec(self):
    #     self.seek_high_mec_stop()
    #     self.seek_low_mec_stop()

    #
    # Methods for seeking end-stop switches is not needed, since
    # torque based seeking of mechanical end-stops works well.
    #
    # def seek_high_end_switch(self, speed=5):
    #     if self.end_switch_high is not None:
    #         while True:
    #             with self._ticket_event:
    #                 self._ticket_event.wait()
    #             if(self._ticket.ios & (1 << self.end_switch_high.pin_address)
    #                == self.end_switch_high.closed_value):
    #                 break
    #             self.vel = np.abs(speed)
    #         self.vel = 0.0
    #         self._p_high = self.pos
    #         if self._p_low is None:
    #             self._p_low = self._p_high - self._stroke
    #         else:
    #             self._stroke = self._p_high - self._p_low
    #     else:
    #         raise Exception('HenschelServo.seek_high_end_switch: ' +
    #                         'High end stop is not defined!')

    # def seek_low_end_switch(self, speed=5):
    #     if self.end_switch_low is not None:
    #         while True:
    #             with self._ticket_event:
    #                 self._ticket_event.wait()
    #             if(self._ticket.ios & (1 << self.end_switch_low.pin_address)
    #                == self.end_switch_low.closed_value):
    #                 break
    #             self.vel = -np.abs(speed)
    #         self.vel = 0.0
    #         self._p_low = self.pos
    #         if self._p_high is None:
    #             self._p_high = self._p_low + self._stroke
    #         else:
    #             self._stroke = self._p_high - self._p_low
    #     else:
    #         raise Exception('HenschelServo.seek_low_end_switch: ' +
    #                         'Low end stop is not defined!')

    def run(self):
        while not self.__stop:
            if self.tcp:
                data = self._sock.recv(1024)
            else:
                data, addr = self._sock.recvfrom(1024)
                self._recv_data = data
                self._recv_addr = addr
            if len(data) == 4 * 33:
                with self._ticket_lock:
                    self._ticket = BinaryTicket(data)
                    with self._ticket_event:
                        self._ticket_event.notify_all()
                # print(f'p = {self._ticket.act_pos:.2f} rad, v={self._ticket.act_vel:.2f}')
                # print(f'ios = {self._ticket.ios:032b}')
                # print(f'v_cmd_cli={self._cmd_vel} v_cmd_mc={self._ticket.cmd_vel}')
                # print(f'mode={self._ticket.mode}')
            elif len(data) == 4 * 4:
                # print('Short ticket')
                with self._ticket_lock:
                    self._ticket = BinaryTicketShort(data)
                    with self._ticket_event:
                        self._ticket_event.notify_all()
            elif len(data) == 85:
                # print('HDrive Ticket')
                with self._ticket_lock:
                    self._ticket = HDriveTicket(data)
                    with self._ticket_event:
                        self._ticket_event.notify_all()
            else:
                print(f'Skipping packet of size {len(data)} ' +
                      f'content:\n{str(data)}')
                self._unhandled_data = data


if __name__ == '__main__':
    # Port seems to be fixed to 1000, even though the web interface
    # says it has been changed. The factory set IP address is
    # 192.168.1.102. Assume that an end switch has been mounted at
    # high position, and that it signals True when closed.
    addr = EtherAddress('192.168.1.102', 1000)
    hs = HenschelServo(addr)  # , end_switch_high=EndSwitch(0, True))
    hs.start()
