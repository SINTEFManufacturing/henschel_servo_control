# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020-2021"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import henschel_servo_control as hsc

addr = hsc.EtherAddress('192.168.1.102', 1000)
hs = hsc.HenschelServo(addr)
hs.start()
