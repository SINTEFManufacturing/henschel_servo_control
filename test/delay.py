# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np
import matplotlib.pyplot as plt

import henschel_servo_control as hsc

addr = hsc.EtherAddress('192.168.1.102', 1000)
hs = hsc.HenschelServo(addr)
hs.start()
hs.get_ticket(wait=True)
# hs.seek_stroke_mec()


T = 10.0
p_cmds = []
p_acts = []

t = t0 = time.time()

a = 10
w = 0.25 * np.pi
p0 = hs.pos
hs.travel_speed = 1000
while t - t0 < T:
    t = time.time()
    ticket = hs.get_ticket(wait=True)
    p_cmd = p0 + a * (1 - np.cos(w*(t-t0)))
    hs.pos = p_cmd
    p_act = ticket.act_pos
    p_cmds.append((t, p_cmd))
    p_acts.append((t, p_act))

hs.pos = p0

plt.plot(*np.transpose(p_cmds), label='p_cmd')
plt.plot(*np.transpose(p_acts), label='p_act')
plt.legend()
plt.grid()
plt.show()
