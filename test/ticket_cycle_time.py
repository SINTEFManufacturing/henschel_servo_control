# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np
import matplotlib.pyplot as plt

import henschel_servo_control as hsc

addr = hsc.EtherAddress('192.168.1.102', 1000)
hs = hsc.HenschelServo(addr)
hs.start()

count = 0
t0 = time.time()
while time.time() < t0 + 2:
    hs.get_ticket(wait=True)
    count += 1
t = time.time()

print(f'Ticket cycle time: {(t-t0)/count:.4f} s')
